import React from "react";
import {Col, Button} from 'react-bootstrap';

import Lang from '../../Lang.js';
const isEn = Lang.isEnglish();

export default class LearnMore extends React.Component{
	render(){
		return(
			<section className="learnmore" style={{background: "url(./images/homepage/bg_StartDoingThings.png)"}}>
				<Col md={9} >
					<div className="description">
						<h1>{isEn ? "Start doing things you used to love":"Recommence à faire les choses que tu aimais"}</h1>
						<p>{isEn ? "Helping you manage your schedule and never miss an opportunity for a client to book an appointment again.":"Nous t'aidons à gérer ton horaire, afin que tu ne manque plus jamais une opportunité pour qu'un client réserve son rendez-vous."}</p>
					</div>
				</Col>
				<Col md={3} >
					<div className="button">
						<Button bsSize="large">{isEn ? "Learn more":"En savoir plus"}</Button>
					</div>
				</Col>
			</section>
		)
	}
}