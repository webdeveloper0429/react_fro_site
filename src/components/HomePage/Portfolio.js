import React from "react";
import {Col, Button} from 'react-bootstrap';

import Lang from '../../Lang.js';
const isEn = Lang.isEnglish();

export default class Portfolio extends React.Component{
	render(){
		return(
			<section className="portfolio">
				<Col xs={4} >
					<div className="item" style={{background: "url(./images/Cities/img_Montreal.jpg)"}}>
						<h1>{isEn ? "Montreal":"Montréal"}</h1>
						<p>&nbsp;</p>
					</div>
				</Col>
				<Col xs={4} >
					<div className="item" style={{background: "url(./images/Cities/img_Toronto.jpg)"}}>
						<h1>{isEn ? "Toronto":"Toronto"}</h1>
						<p>{isEn ? "Coming Soon":"Arrive Bientôt"}</p>
					</div>
				</Col>
				<Col xs={4} >
					<div className="item" style={{background: "url(./images/Cities/img_Ottawa.jpg)"}}>
						<h1>{isEn ? "Ottawa":"Ottawa"}</h1>
						<p>{isEn ? "Coming Soon":"Arrive Bientôt"}</p>
					</div>
				</Col>
			</section>
		)
	}
}