import React from "react";
import {Col, Button} from 'react-bootstrap';

import NavBar from '../common/NavBar.js';
import Footer from '../common/Footer.js';
import Banner from './Banner.js';
import Posts from './Posts.js';
import LearnMore from './LearnMore.js';
import Portfolio from './Portfolio.js';
import Subscribe from './Subscribe.js';

export default class HomePage extends React.Component {
	componentDidMount() {
		$("nav .btn").addClass('btn-orange');
	}
	render() {
		return (
			<div className="page_container">
				<NavBar />
				<div className="homepage">
					<Banner />
					<Posts />
					<LearnMore />
					<Portfolio />
					<Subscribe />
				</div>
				<Footer />
			</div>
		);
	}
}
