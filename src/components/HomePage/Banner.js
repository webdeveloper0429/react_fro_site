import React from "react";
import {Link} from "react-router";
import {Col, Button} from 'react-bootstrap';

import Lang from '../../Lang.js';
const isEn = Lang.isEnglish();

export default class Banner extends React.Component{
	constructor(props) {
		super(props);
		this.gotoDown = this.gotoDown.bind(this);
	}
	gotoDown(e){
		e.preventDefault();
		const offset = $($('nav')[0]).height();
		const hash = $(".down_arrow a")[0].hash;
		$('html, body').animate({
			scrollTop: $(hash).offset().top-offset
		}, 800, function(){
			window.location.hash = hash;
		});
	}
	render(){
		return(
			<section className="banner">
				<Col md={6} >
					<div className="banner_left">
						<div className="logoImg">
							<img src="./images/homepage/final_logo_white.png" width="120" height="120"/>
						</div>
						<div className="title">
							{isEn ? "Great beauty experiences start here":"Une bonne expérience de beauté débute ici"}
						</div>
						<div className="mobileIcons">
							<Link to="/"><img src="./images/homepage/apple.png" width="80" height="80"/></Link>
							<Link to="/"><img src="./images/homepage/android.png" width="80" height="80"/></Link>
						</div>
					</div>
				</Col>
				<Col md={6} >
					<div className="banner_right">
						<img src="./images/homepage/double_mobile.png" width="400" height="450"/>
					</div>
				</Col>
				<Col md={12} >
					<div className="down_arrow">
						<a href="#posts" onClick={this.gotoDown}><img src="./images/homepage/down_arrow.png" width="50" height="50"/></a>
					</div>
				</Col>
			</section>
		)
	}
}