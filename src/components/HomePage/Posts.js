import React from "react";
import {Col, Button} from 'react-bootstrap';

import Lang from '../../Lang.js';
const isEn = Lang.isEnglish();

export default class Posts extends React.Component{
	render(){
		return(
			<section id="posts" className="post">
				<Col md={4} >
					<div className="item">
						<div className="title">
							<i className="fa fa-search" aria-hidden="true"></i> {isEn ? "Find services you will love":"Trouve des services que tu aimes"}
						</div>
						<div className="content">
							{isEn ? "Easily find local beauty professionals. View their profile, read customer reviews and book the best service taht fit your needs.":"Recherche facilement des professionels de la beauté local.  Regarde leur profil, lis les commentaires clients et réserve le service adapté à tes besoins."}
						</div>
					</div>
				</Col>
				<Col md={4} >
					<div className="item">
						<div className="title">
							<i className="fa fa-calendar" aria-hidden="true"></i> {isEn ? "Book appointment 24/7":"Réserve un rendez-vous 24/7"}
						</div>
						<div className="content">
							{isEn ? "Select one or more services, view availability and book an appointment smoothly.  Get email and sms reminders before your appointment.":"Sélectionne un ou plusieurs services, regarde les disponibilités et réserve en ligne sans effort.  Reçois des rappels via courriel et sms avant ton rendez-vous."}
						</div>
					</div>
				</Col>
				<Col md={4} >
					<div className="item">
						<div className="title">
							<i className="fa fa-star-o" aria-hidden="true"></i> {isEn ? "Share your experience":"Partage ton expérience"}
						</div>
						<div className="content">
							{isEn ? "Enjoy your beauty appointment and let other people know about your experience by posting an helpful review.":"Profite de ton rendez-vous et laisse les autres apprendre de ton expérience en partageant tes commentaires."}
						</div>
					</div>
				</Col>
			</section>
		)
	}
}