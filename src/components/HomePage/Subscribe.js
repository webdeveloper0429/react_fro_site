import React from "react";
import {Col, FormGroup, Button, InputGroup, FormControl, HelpBlock} from 'react-bootstrap';

import Lang from '../../Lang.js';
const isEn = Lang.isEnglish();

export default class Subscribe extends React.Component{
	constructor(props) {
		super(props);
		this.stepInside = this.stepInside.bind(this);
		this.state={
			emailHelp: ''
		}
	}
	stepInside(){
		if (!$('#email').val()) {
			this.setState({emailHelp: 'Email is required!'});
			return false;
		} else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test($('#email').val())) {
			this.setState({emailHelp: 'Invalid email address!'});
			return false;
		}
		else this.setState({emailHelp: ''});
	}
	render(){
		return(
			<section className="subscribe">
				<h1 className="title">
					{isEn ? "Be the first to know when Fro comes near you":"Sois le premier à savoir quand Fro arrive dans ta ville"}
				</h1>
				<div className="email">
					
					<FormGroup bsSize="large">
						<InputGroup>
							<FormControl type="email" placeholder="email" id="email"/>
							<InputGroup.Button>
								<Button bsSize="large" bsStyle="blue-green" onClick={this.stepInside}>{isEn ? "Step Inside":"Entre Ici"}</Button>
							</InputGroup.Button>
						</InputGroup>
						{this.state.emailHelp && <HelpBlock>{this.state.emailHelp}</HelpBlock>}
					</FormGroup>
					
				</div>
			</section>
		)
	}
}