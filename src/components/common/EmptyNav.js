import React from "react";
import { Link } from 'react-router';
import { Navbar } from 'react-bootstrap';

export default class NavBar extends React.Component {
	render() {
		return (
			<Navbar fixedTop>
				<Navbar.Header>
					<Navbar.Brand>
						<Link to="/">
							<div style={{background: 'url(./images/logo.png)'}} className="logo"></div>
						</Link>
					</Navbar.Brand>
					<Navbar.Toggle />
				</Navbar.Header>
			</Navbar>
		);
	}
}