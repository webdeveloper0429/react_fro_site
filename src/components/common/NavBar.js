import React from "react";
import { Link } from 'react-router';
import { Navbar, Nav, NavItem, NavDropdown, MenuItem, Button } from 'react-bootstrap';

import Lang from '../../Lang.js';
const isEn = Lang.isEnglish();

export default class NavBar extends React.Component {
	render() {
		return (
			<Navbar fluid fixedTop>
				<Navbar.Header>
					<Navbar.Brand>
						<Link to="/">
							<div style={{background: 'url(./images/logo.png)'}} className="logo"></div>
						</Link>
					</Navbar.Brand>
					<Navbar.Toggle />
				</Navbar.Header>
				<Navbar.Collapse>
					{ isEn ?
						<Nav pullRight>
							<NavItem eventKey={3} href="/download">Download</NavItem>
							<NavItem eventKey={2} href="/login">Log in</NavItem>
							<NavItem eventKey={1} href="/proinfo">
								<Button bsStyle="blue-green">Beauty Professional</Button>
							</NavItem>
						</Nav>
					:
						<Nav pullRight>
							<NavItem eventKey={3} href="/download">Télécharge</NavItem>
							<NavItem eventKey={2} href="/login">Connexion</NavItem>
							<NavItem eventKey={1} href="/proinfo">
								<Button bsStyle="blue-green">Pro de la Beauté</Button>
							</NavItem>
						</Nav>
					}
				</Navbar.Collapse>
			</Navbar>
		);
	}
}
