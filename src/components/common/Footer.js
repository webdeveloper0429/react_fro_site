import React from "react";
import {Link} from "react-router";
import {Col, Button} from 'react-bootstrap';

import Lang from '../../Lang.js';
const isEn = Lang.isEnglish();

export default class Footer extends React.Component{
	toEn(e){
		e.preventDefault();
		Lang.setEnglish();
		location.reload();
	}
	toFr(e){
		e.preventDefault();
		Lang.setFrance();
		location.reload();
	}
	render(){
		return(
			<section className="footer">
				<Col md={6} >
					<Col md={12} >
						<h4 className="title">{isEn ? <span>Follow us!  We're friendly:</span>:<span>Suis-nous!  Nous sommes amicaux:</span>} &nbsp;&nbsp;&nbsp;&nbsp;
							<a target="_blank" href="https://www.facebook.com/FroApp/"><i className="fa fa-facebook-f social_icons"></i></a>&nbsp;&nbsp;•&nbsp;&nbsp;
							<a target="_blank" href="https://twitter.com/froapp"><i className="fa fa-twitter social_icons"></i></a>&nbsp;&nbsp;•&nbsp;&nbsp;
							<a target="_blank" href="https://www.instagram.com/froapp/"><i className="fa fa-instagram social_icons"></i></a>
						</h4>
					</Col>
					<Col md={12} >
						<div className="footer_menu menu_left">
							<h4 className="title"> {isEn ? <span>Discover</span>:<span>Découvre</span>} </h4>
							<ul>
								<li> <a href="/proinfo"> {isEn ? <span>Beauty Professional</span>:<span>Pro de la Beauté</span>} </a> </li>
								<li> <a href="/stylistprofile"> {isEn ? <span>Fro Elite</span>:<span>Fro Élite</span>} </a> </li>
								<li> <a href="/download"> {isEn ? <span>Download</span>:<span>Télécharge</span>} </a> </li>
								<li> <a href="/help"> {isEn ? <span>Help</span>:<span>Aide</span>} </a> </li>
							</ul>
						</div>
						<div className="footer_menu menu_right">
							<h4 className="title"> {isEn ? <span>Company</span>:<span>Compagnie</span>} </h4>
							<ul>
								<li> <a href="/ourstory"> {isEn ? <span>Our Story</span>:<span>Notre Histoire</span>} </a> </li>
								<li> <a href="/contactus"> {isEn ? <span>Contact Us</span>:<span>Contacte-nous</span>} </a> </li>
								<li> <a href="#"> {isEn ? <span>Terms & Privacy</span>:<span>Termes & Confidentialité</span>} </a> </li>
							</ul>
						</div>
					</Col>
				</Col>
				<Col md={6} >
					<Col md={6} mdOffset={3}>
						{isEn?
							<h4 className="lang">
								<span className="lang" onClick={this.toEn}>English</span>&nbsp;&nbsp;•&nbsp;&nbsp;<span className="lang active" onClick={this.toFr}>Français</span>
							</h4>
						:
							<h4 className="lang">
								<span className="lang active" onClick={this.toEn}>English</span>&nbsp;&nbsp;•&nbsp;&nbsp;<span className="lang" onClick={this.toFr}>Français</span>
							</h4>
						}
						<br />
						<h4 className="title"> {isEn ? <span>Download our app</span>:<span>Télécharge notre app</span>} </h4>
						<p>{isEn ? <span>Search, Compare and Book beauty appointment on the go</span>:<span>Recherche, Compare et Réserve des services de beauté où que tu es</span>}</p>
						<div className="">
							<Link to="/"><img src="./images/footer/ic_apple_grey.png" width="70" height="70"/></Link>
							<Link to="/"><img src="./images/footer/ic_anroid_grey.png" width="70" height="70"/></Link>
						</div>
					</Col>
				</Col>
			</section>
		)
	}
}