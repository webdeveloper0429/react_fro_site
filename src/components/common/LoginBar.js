import React from "react";
import { Link } from 'react-router';
import { Navbar, Nav, NavItem } from 'react-bootstrap';

import Lang from '../../Lang.js';
const isEn = Lang.isEnglish();

export default class NavBar extends React.Component {
	render() {
		return (
			<Navbar fixedTop>
				<Navbar.Header>
					<Navbar.Brand>
						<Link to="/">
							<div style={{background: 'url(./images/logo.png)'}} className="logo"></div>
						</Link>
					</Navbar.Brand>
					<Navbar.Toggle />
				</Navbar.Header>
				<Navbar.Collapse>
					<Nav pullRight>
						<span eventKey={2} className="navbar-text">{isEn ? "Already have an account?":"Tu as déjà un compte?"}</span>
						<NavItem eventKey={1} href="/login"><span className='blueGreentext'>{isEn ? "Log in":"Connectes-toi"}</span></NavItem>
					</Nav>
				</Navbar.Collapse>
			</Navbar>
		);
	}
}