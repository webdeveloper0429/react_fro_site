let data = 
{
	"events":[
		{
			// 'title': 'All Day Event',
			'allDay': true,
			'start': new Date(2017, 4, 0),
			'end': new Date(2017, 4, 1)
		},
		{
			// 'title': 'Long Event',
			'start': new Date(2017, 4, 7),
			'end': new Date(2017, 4, 10)
		},

		{
			// 'title': 'DTS STARTS',
			'start': new Date(2016, 2, 13, 0, 0, 0),
			'end': new Date(2016, 2, 20, 0, 0, 0)
		},

		{
			// 'title': 'DTS ENDS',
			'start': new Date(2016, 10, 6, 0, 0, 0),
			'end': new Date(2016, 10, 13, 0, 0, 0)
		},

		{
			// 'title': 'Some Event',
			'start': new Date(2017, 4, 9, 0, 0, 0),
			'end': new Date(2017, 4, 9, 0, 0, 0)
		},
		{
			// 'title': 'Conference',
			'start': new Date(2017, 4, 11),
			'end': new Date(2017, 4, 13),
			desc: 'Big conference for important people'
		},
		{
			// 'title': 'Meeting',
			'start': new Date(2017, 4, 12, 10, 30, 0, 0),
			'end': new Date(2017, 4, 12, 12, 30, 0, 0),
			desc: 'Pre-meeting meeting, to prepare for the meeting'
		},
		{
			// 'title': 'Meeting',
			'start':new Date(2017, 4, 12,14, 0, 0, 0),
			'end': new Date(2017, 4, 12,15, 0, 0, 0)
		},
		{
			// 'title': 'Happy Hour',
			'start':new Date(2017, 4, 12, 17, 0, 0, 0),
			'end': new Date(2017, 4, 12, 17, 30, 0, 0),
			desc: 'Most important meal of the day'
		},
		{
			// 'title': 'Dinne/r',
			'start':new Date(2017, 4, 12, 20, 0, 0, 0),
			'end': new Date(2017, 4, 12, 21, 0, 0, 0)
		},
		{
			// 'title': 'Birthday Party',
			'start':new Date(2017, 4, 13, 7, 0, 0),
			'end': new Date(2017, 4, 13, 10, 30, 0)
		}
	],
	"FAQ_En":[
		{
			"Q": "How much does it cost?",
			"A": "It is free."
		},{
			"Q": "How long does it take to create a beauty pro profile?",
			"A": "Less than 5 minutes"
		},{
			"Q": "As a beauty pro, do I need a physical location or a salon?",
			"A": "No. Fro is for enterprising women who want to earn extra money. It doesn't matter if you work at a salon, from your house or drive to your client's place."
		},{
			"Q": "Where are you launching?",
			"A": "We are officially launched in Greater Montreal .  We'll be launching soon in Toronto and Ottawa."
		},{
			"Q": "How do I book an appointment on Fro?",
			"A": "Through the beauty pro's profile.  You can access it through their unique link or directly on our app."
		},{
			"Q": "Do you display other services besides hair styling?",
			"A": "Yes.  We have now added makeup artists as well."
		},{
			"Q": "I had a problem with my service, who do I contact?",
			"A": "Beauty Pro, as a separate entity from Fro, are solely responsible for all customer service issues, including pricing, service fulfillment, appointment cancellation, refunds and adjustments, etc.For any other issues, please contact Kadiatou (support@froapp.ca) immediately to share your concerns. Make sure to include your name and phone number in your email."
		},{
			"Q": "Can a beauty pro come to my home or office?",
			"A": "If a Beauty Pro offers the service of travelling at a client's place it is displayed with the price on her profile.  When booking, simply select the desired option and enter your address. Please select a Beauty Pro that is no further than a 20 minutes drive away."
		},{
			"Q": "What if I need to cancel my appointment?",
			"A": "If you have any changes in your schedule and need to reschedule your appointment, simply message your Beauty Pro and cancel it from your appointment list."
		}
	],
	"FAQ_Fr":[
		{
			"Q": "Combien ça coûte?",
			"A": "C'est gratuit."
		},{
			"Q": "Combien de temps faut-il pour créer un profil de Pro?",
			"A": "Moins de 5 minutes"
		},{
			"Q": "En tant que Pro, ai-je besoin d'un emplacement physique ou d'un salon?",
			"A": "Non. Fro est pour les femmes entreprenantes qui veulent gagner de l'argent supplémentaire. Peu importe si vous travaillez dans un salon, de votre maison ou si vous vous déplacez chez vos clients."
		},{
			"Q": "Dans quelles villes êtes-vous présent?",
			"A": "Nous sommes présents dans le Grand Montréal.  Nous lancerons bientôt à Toronto et Ottawa."
		},{
			"Q": "Comment réserver un rendez-vous sur Fro?",
			"A": "Sur le profil des Pro de la beauté accessible via un lien unique ou directement sur notre application mobile."
		},{
			"Q": "Affichez-vous d'autres services que ceux de coiffures?",
			"A": "Oui.  Nous avons ajouté l'option d'afficher des services de maquillages."
		},{
			"Q": "J'ai eu un problème avec mon service, qui dois-je contacter?",
			"A": "Une Pro de la beauté est une entité distincte de Fro et est seule responsable de tous les problèmes de service à la clientèle, y compris le prix, l'exécution du service, l'annulation du rendez-vous, les remboursements et les ajustements, etc. Pour toute autre question, communiquez immédiatement avec Kadiatou (support@froapp.ca) pour nous faire part de vos préoccupations. Assurez-vous d'inclure votre nom et votre numéro de téléphone dans votre courriel."
		},{
			"Q": "Est-ce qu'une Pro peut venir chez moi ou à mon bureau?",
			"A": "Si une Pro de la beauté offre le service de se déplacer chez une cliente, l'information est affichée avec le prix sur son profil.  Lors de la réservation, il suffit de sélectionner l'option souhaitée et d'inscrire votre adresse.  Merci de choisir une Pro qui est à moins de 20 minutes en voiture."
		},{
			"Q": "Que faire si j'ai besoin d'annuler mon rendez-vous",
			"A": "Si vous avez un changement d'horaire et que vous devez reporter votre rendez-vous, envoyez simplement un message à votre Pro et annulez-le de votre liste de rendez-vous."
		}
	]
}
export default data;