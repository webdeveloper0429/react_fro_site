import React from "react";
import {Col, Button} from 'react-bootstrap';

import NavBar from '../common/NavBar.js';
import Footer from '../common/Footer.js';

import Lang from '../../Lang.js';
const isEn = Lang.isEnglish();

export default class Download extends React.Component {
	render() {
		return (
			<div className="page_container">
				<NavBar />
				<div className="download">
					<div className="banner">
						<div className="content">
							<Col sm={6}>
								<img src="./images/download/download_phone.png" width="244" height="500"/>
							</Col>
							<Col sm={6}>
								<h1>{isEn ? "Download Now":"Télécharge Maintenant"}</h1>
								<div className="mobileIcons">
									<div className="imgbtn"><img src="./images/download/apple.png" width="90" height="90"/></div>
									<div className="imgbtn"><img src="./images/download/android.png" width="90" height="90"/></div>
								</div>
							</Col>
						</div>
					</div>
				</div>
				<Footer />
			</div>
		);
	}
}
