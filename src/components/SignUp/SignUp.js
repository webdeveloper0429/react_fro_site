import React from "react";
import { Link, browserHistory } from 'react-router';
import {Col, Checkbox, Button, FormGroup, ControlLabel, FormControl, HelpBlock} from 'react-bootstrap';

import LoginBar from '../common/LoginBar.js';
import Footer from '../common/Footer.js'

import Lang from '../../Lang.js';
const isEn = Lang.isEnglish();

function FieldGroup({ id, label, help, ...props }) {
  return (
	<FormGroup controlId={id}>
	  <FormControl {...props} />
	  {help && <HelpBlock>{help}</HelpBlock>}
	</FormGroup>
  );
}
export default class SignUp extends React.Component {
	constructor(props) {
		super(props);
		this.getStarted = this.getStarted.bind(this);
		this.state={
			firstNameHelp: '',
			lastNameHelp: '',
			emailHelp: '',
			passwordHelp: '',
		}
	}
	getStarted(){
		if (!$('#firstName').val()) {
			this.setState({firstNameHelp: 'First Name is required!'});
			return false;
		}
		else this.setState({firstNameHelp: ''});

		if (!$('#lastName').val()) {
			this.setState({lastNameHelp: 'Last Name is required!'});
			return false;
		}
		else this.setState({lastNameHelp: ''});

		if (!$('#email').val()) {
			this.setState({emailHelp: 'Email is required!'});
			return false;
		} else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test($('#email').val())) {
			this.setState({emailHelp: 'Invalid email address!'});
			return false;
		}
		else this.setState({emailHelp: ''});

		if (!$('#password').val()) {
			this.setState({passwordHelp: 'Password is required!'});
			return false;
		} else if ($('#password').val().length < 8) {
			this.setState({passwordHelp: 'Password should be more than 8 characters!'});
			return false;
		}
		else this.setState({passwordHelp: ''});

		browserHistory.push('/getstarted');
	}
	render() {
		return (
			<div className="page_container">
				<LoginBar />
				<div className="login">
					<div className="bg">
						<div className="title">
							<h1>{isEn ? "Create a Free Client Account":"Crée un Compte Client Gratuit"}</h1>
							<h5>{isEn ? "Looking for work?":"Pour travailler?"} <Link to="/">{isEn ? "Sign up as a beauty professional":"Inscris-toi comme pro de la beauté"}</Link> </h5>
						</div>
						<br/><br/>
						<div className="contact_form">
							<form>
								<FormGroup>
									<Button bsSize="large" bsStyle="facebook" block>Facebook</Button>
								</FormGroup>
								<FormGroup>
									<Button bsSize="large" bsStyle="google" block>Google</Button>
								</FormGroup>
								<br/><p style={{textAlign: 'center'}}>{isEn ? "or":"ou"}</p><br/>
								<FieldGroup
									id="firstName"
									type="text"
									placeholder={isEn ? "First Name":"Prénom"}
									bsSize="large"
									help={this.state.firstNameHelp}
								/>
								<FieldGroup
									id="lastName"
									type="text"
									placeholder={isEn ? "Last Name":"Nom"}
									bsSize="large"
									help={this.state.lastNameHelp}
								/>
								<FormGroup>
									<FormControl componentClass="select" bsSize="large">
										<option value="canada">Canada</option>
										<option value="unitedstate">{isEn ? "United States":"États-Unis"}</option>
									</FormControl>
								</FormGroup>
								<FieldGroup
									id="email"
									type="email"
									placeholder={isEn ? "Email":"Courriel"}
									bsSize="large"
									help={this.state.emailHelp}
								/>
								<FieldGroup
									id="password"
									type="password"
									placeholder={isEn ? "Password":"Mot de passe"}
									bsSize="large"
									help={this.state.passwordHelp}
								/>
								<FormGroup>
									<Checkbox>
										{isEn ? "Yes!  Send me genuinely useful emails every now and then to help me get the most out of Fro.":"Oui!  Envois-moi des courriels réellement utiles une fois de temps à autre pour bénéficier davantage de Fro."}
									</Checkbox>
								</FormGroup>
								<FormGroup>
									<Checkbox>
										{isEn ? "Yes!  I understand and agree to the Fro":"Oui!  Je comprends et accepte les "} <Link to="/">{isEn ? "Terms of Service":"Termes de Service"}</Link>.
										{isEn ? "and":"et la"} <Link to="/">{isEn ? "Privacy Policy":"Politique de confidentialité"}</Link>.
									</Checkbox>
								</FormGroup>
								<br/><br/>
								<FormGroup>
									<Button bsSize="large" bsStyle="blue-green" block onClick={this.getStarted}>{isEn ? "Get Started":"Commencer"}</Button>
								</FormGroup>
							</form>
						</div>
					</div>
				</div>
				<Footer />
			</div>
		);
	}
}
