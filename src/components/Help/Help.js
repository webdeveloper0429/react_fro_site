import React from "react";
import {Col, Accordion, Panel} from 'react-bootstrap';

import NavBar from '../common/NavBar.js';
import Footer from '../common/Footer.js';

import Lang from '../../Lang.js';
const isEn = Lang.isEnglish();

import Data from '../common/Data.js';

export default class Help extends React.Component {
	render_FAQ(){
		// isEn? var FAQ = Data.FAQ_En : var FAQ = Data.FAQ_Fr;
		if(isEn) var FAQ = Data.FAQ_En;
		else var FAQ = Data.FAQ_Fr
		return FAQ.map((item, key)=>
			<div key={key}>
				<Accordion >
					<Panel header={item.Q} eventKey="1">
						{item.A}
					</Panel>
				</Accordion>
			</div>
		)
	}
	render() {
		return (
			<div className="page_container">
				<NavBar />
				<div className="help">
					<div className="bg">
						<div className="title">
							<h1>{isEn ? "FREQUENTLY ASKED QUESTIONS":"QUESTIONS FRÉQUEMMENT POSÉES"}</h1>
							<h4>{isEn ? "We've gathered together the most common questions and answered them as best we can. ":"Nous avons rassemblé les questions les plus courantes et avons répondu de la meilleure façon possible. "}</h4>
							<hr width="200"/>
						</div>
						<div className="help_panel">
							{this.render_FAQ()}
						</div>
					</div>
				</div>
				<Footer />
			</div>
		);
	}
}
