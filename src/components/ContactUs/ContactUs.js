import React from "react";
import {Col, Button, FormGroup, ControlLabel, FormControl, HelpBlock} from 'react-bootstrap';
import Recaptcha from'react-gcaptcha';

import NavBar from '../common/NavBar.js';
import Footer from '../common/Footer.js';

import Lang from '../../Lang.js';
const isEn = Lang.isEnglish();

function FieldGroup({ id, label, help, ...props }) {
  return (
	<FormGroup controlId={id}>
	  <FormControl {...props} />
	  {help && <HelpBlock>{help}</HelpBlock>}
	</FormGroup>
  );
}

var callback = function (key) {
  console.log(key);
};
var loaded = function () {
  console.log('recaptchaLoaded');
};
export default class ContactUs extends React.Component {
	constructor(props) {
		super(props);
		this.onSubmit = this.onSubmit.bind(this);
		this.state={
			nameHelp: '',
			subjectHelp: '',
			emailHelp: '',
			messageHelp: '',
		}
	}
	onSubmit(e){
		if (!$('#name').val()) {
			this.setState({nameHelp: 'Name is required!'});
			return false;
		}
		else this.setState({nameHelp: ''});

		if (!$('#subject').val()) {
			this.setState({subjectHelp: 'Subject is required!'});
			return false;
		}
		else this.setState({subjectHelp: ''});

		if (!$('#email').val()) {
			this.setState({emailHelp: 'Email is required!'});
			return false;
		} else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test($('#email').val())) {
			this.setState({emailHelp: 'Invalid email address!'});
			return false;
		}
		else this.setState({emailHelp: ''});

		if (!$('#message').val()) {
			this.setState({messageHelp: 'Message is required!'});
			return false;
		}
		else this.setState({messageHelp: ''});

		if ($("#g-recaptcha-response").val() === "") {
			e.preventDefault();
			alert("Please check the recaptcha");
		}
	}
	render() {
		return (
			<div className="page_container">
				<NavBar />
				<div className="contactus">
					<div className="bg">
						<div className="title">
							<h1>{isEn ? "Contact Us":"Nous Contacter"}</h1>
							<h4>{isEn ? "We'd love to hear from you.  Please feel free to contact us.":"Nous serions ravis de vous entendre.   N'hésitez pas à nous contacter en utilisant ce formulaire."}</h4>
							<hr width="200"/>
						</div>
						<div className="contact_form">
							<form>
								<FieldGroup
									id="name"
									type="text"
									placeholder={isEn ? "Name":"Nom"}
									bsSize="large"
									help={this.state.nameHelp}
								/>
								<FieldGroup
									id="subject"
									type="text"
									placeholder={isEn ? "Subject":"Sujet"}
									bsSize="large"
									help={this.state.subjectHelp}
								/>
								<FieldGroup
									id="email"
									type="email"
									placeholder={isEn ? "Email Address":"Courriel"}
									bsSize="large"
									help={this.state.emailHelp}
								/>
								<FieldGroup
									componentClass="textarea"
									id="message"
									rows="5"
									placeholder={isEn ? "Message":"Message"}
									bsSize="large"
									help={this.state.messageHelp}
								/>
								<FormGroup>
									<Recaptcha
										sitekey='6LcxIiAUAAAAAFiiWWKVkDWJFmLd4WdKyMLueRNY'
										onloadCallback={loaded}
										verifyCallback={callback}
									/>
								</FormGroup>
								<FormGroup>
									<Button bsSize="large" bsStyle="blue-green" block onClick={this.onSubmit}>{isEn ? "Submit":"Envoyer"}</Button>
								</FormGroup>
								<FormGroup>
									<Button bsSize="large" bsStyle="dimGray" block>{isEn ? "Reset":"Réinitialiser"}</Button>
								</FormGroup>
							</form>
						</div>
					</div>
				</div>
				<Footer />
			</div>
		);
	}
}
