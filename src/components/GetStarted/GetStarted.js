import React from "react";
import {Col, Button} from 'react-bootstrap';
import { browserHistory } from 'react-router';

import LoginBar from '../common/LoginBar.js';
import Footer from '../common/Footer.js';

import Lang from '../../Lang.js';
const isEn = Lang.isEnglish();

export default class GetStarted extends React.Component {
	startBook(){
		browserHistory.push('/signup');
	}
	render() {
		return (
			<div className="page_container">
				<LoginBar />
				<div className="getstarted">
					<div className='title'>
						<h1>{isEn ? "Let's get started!":"Commençons!"}</h1>
						<h4>{isEn ? "First, tell us what you're looking for":"Premièrement, dis-nous ce que tu recherches"}</h4>
					</div>
					<div className='content'>
						<Col md={5}>
							<div className='item'>
								<h4>{isEn ? "I want to book a beauty professional":"Je veux réserver une pro de la beauté"}</h4>
								<p>{isEn ? "Find, browse profiles and book appointments":"Trouve, parcours les profils et prends rendez-vous"}</p>
								<Button bsSize='large' bsStyle='blue-green' onClick={this.startBook}>{isEn ? "Book":"Réserve"}</Button>
							</div>
						</Col>
						<Col md={2}>
							<div className='or'>
								{isEn ? "OR":"OU"}
							</div>
						</Col>
						<Col md={5}>
							<div className='item'>
								<h4>{isEn ? "I'm looking to list my services":"Je veux afficher mes services"}</h4>
								<p>{isEn ? "Grow your business by providing 24/7 online booking":"Développe ton entreprise en offrant la réservation en ligne 24/7"}</p>
								<Button bsSize='large' bsStyle='blue-green'>{isEn ? "List":"Affiche"}</Button>
							</div>
						</Col>
					</div>
				</div>
				<Footer />
			</div>
		);
	}
}
