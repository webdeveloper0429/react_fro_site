import React from "react";
import {Col, ResponsiveEmbed, Button} from 'react-bootstrap';

import NavBar from '../common/NavBar.js';
import Footer from '../common/Footer.js';

import Lang from '../../Lang.js';
const isEn = Lang.isEnglish();

export default class OurStory extends React.Component {
	componentDidMount() {
		$("nav .btn").addClass('btn-orange');
	}
	render() {
		return (
			<div className="page_container">
				<NavBar />
				<div className="ourstory">
					<section className="banner">
						<div className="title">
							<h2>{isEn ? "Our Mission":"Notre Mission"}</h2>
							<h1>{isEn ? "To build a genuine, human and community-based marketplace.":"Bâtir un marché authentique, humain et centré sur la communauté."}</h1>
						</div>
					</section>
					<section className="description">
						<div className="title">
							<h2>{isEn ? "Our Story":"Notre Histoire"}</h2>
						</div>
						<div className="content">
							{isEn ? "Fro was founded in December 2016 as a practical solution to an age-old problem: finding the perfect hair stylist.  Over the past 10 years, Kadiatou and her friends endured money-wasting trial-and-error, put up with bad service and long waits… all in the name of beauty.  There had to be a better way and one evening, Kadiatou and David had a aha moment.  We developed Fro to fill that void, with the goal of building the easiest, most convenient and reliable way for black women everywhere to book beauty services.  ":"Fro a été fondé en décembre 2016 comme solution pratique à un problème commun: trouver la coiffeuse parfaite. Au cours des 10 dernières années, Kadiatou et ses amies ont gaspillé beaucoup d'argent en essaies-erreur, ont vécu du mauvais service à la clientèle et de longues attentes... tout au nom de la beauté. Il devait y avoir une meilleure façon de faire et, un soir, Kadiatou et David ont eu une révélation. Nous avons développé Fro pour combler ce vide, dans le but de construire la manière la plus simple, la plus pratique et la plus fiable pour les femmes noires partout dans le monde de réserver des services de beauté."}
						</div>
					</section>
					<section className="founders">
						<div className="title">
							<h2>{isEn ? "Our Founders":"Nos Fondateurs"}</h2>
						</div>
						<div className="content">
							<div className="photo">
								<ResponsiveEmbed a16by9>
									<embed src="./images/ourstory/_MG_8668-2.jpg" />
								</ResponsiveEmbed>
							</div>
							<Col md={7}>
								<div className="item">
									<h3>{isEn ? "David L'Heureux, CEO":"David L'Heureux, CEO"}</h3>
									<div className="introduce">
										{isEn ? "Prior to founding Fro, David founded My Smart Waiter,  a mobile app allowing guests to easily order food & beverage at casual restaurants and sports bars.  He studied psychology at the University of Montreal and valorizes continuous learning.  According to him, learning and innovation are perhaps the most important ingredients of any successful business.":"Avant de fonder Fro, David a fondé My Smart Waiter, une application mobile permettant aux clients de commander facilement de la nourriture et des boissons dans des restaurants décontractés et des bars sportifs. Il a étudié la psychologie à l'Université de Montréal et valorise l'apprentissage continu. Selon lui, l'apprentissage et l'innovation sont possiblement les ingrédients les plus importants de toute entreprise prospère."}
									</div>
								</div>
								<div className="item">
									<h3>{isEn ? "Kadiatou Haïdara, CCO":"Kadiatou Haïdara, CCO"}</h3>
									<div className="introduce">
										{isEn ? "Kadiatou graduated in accounting from UQAM and can really relate to the struggle experienced by so many clients.  She worked for different organization focused on the community and to make extra money during her university years, she even offered hair styling services to her friends.  ":"Kadiatou est diplômé en comptabilité de l'UQAM et comprend réellement les problématiques de service à la clientèle vécus par tant de clientes. Elle a travaillé pour différentes organisations axées sur la communauté et pour gagner de l'argent supplémentaire pendant ses années d'université, elle a même offert des services de coiffure à ses amies."}
									</div>
								</div>
								<div className="item">
									<h3>{isEn ? "Tidiane Seri-Gnoleba, CTO":"Tidiane Seri-Gnoleba, CTO"}</h3>
									<div className="introduce">
										{isEn ? "Tidiane holds a master's degree in software engineering from Polytechnique of Montreal.   He has always been passionate about computer science and constantly desires to learn new technologies, programming languages, frameworks to implement a given task in an efficient and innovative manner.":"Tidiane détient une maîtrise en génie logiciel de la Polytechnique de Montréal. Il a toujours été passionné d'informatique et désire toujours apprendre de nouvelles technologies, des langages de programmation, des 'framework' pour mettre en œuvre une tâche donnée de manière plus efficace."}
									</div>
								</div>
								<div className="item">
									<h3>{isEn ? "JF Lanoue, VP Engineering ":"JF Lanoue, VP Engineering"}</h3>
									<div className="introduce">
										{isEn ? "Prior to founding Fro, JF founded several technology companies, including Revolusoft and Relu, a marketplace that allows students to sell and buy second-hand textbooks.   His 10 years of experience have enabled him to develop an expertise in IT infrastructure, database management and SEO.":"Avant de fonder Fro, JF a fondé plusieurs sociétés en technologie, dont Revolusoft et Relu, un marché qui permettait aux étudiants de vendre et d'acheter des manuels scolaires usagés. Ses 10 ans d'expériences, lui ont permis de développer une expertise en infrastructure informatique, gestion de bases de données et en  référencement."}
									</div>
								</div>
							</Col>
							<Col md={5}>
								<div className="presskit">
									<h3>{isEn ? "Press Kit":"Info Presse"}</h3>
									<br/>
									<img src="./images/ourstory/ourstory_phone.png" width="143" height="172" />
									<br/>
									<Button bsSize="large" bsStyle="orange">{isEn ? "Download press kit":"Télécharge "}</Button>
									<br/>
									<div className="text">
										{isEn ? "For media inquiries,":"Pour les médias, "}
										{isEn ? "please contact ":"contactez-nous à"}
									</div>
									<br/>
									<div className="text orange">
										press@froapp.com
									</div>
									<br/><br/><br/>
									<h3>{isEn ? "Connect With Us":"Suis-nous sur"}</h3>
									<h4 className="social_icons">
										<a href="#"><i className="fa fa-facebook-f social_icons"></i></a>&nbsp;&nbsp;•&nbsp;&nbsp;
										<a href="#"><i className="fa fa-twitter social_icons"></i></a>&nbsp;&nbsp;•&nbsp;&nbsp;
										<a href="#"><i className="fa fa-instagram social_icons"></i></a>
									</h4>
								</div>
							</Col>
						</div>
					</section>
					<section className="join">
						<div className="box">
							<h2>{isEn ? "Join Our Team":"Rejoins Notre Équipe"}</h2>
							<div className="join_text">
								{isEn ? "We're making a difference in the lives of businesses and consumers and loving every minutes of it.":"Nous faisons une différence dans la vie d'entreprises et des consommateurs et nous l'apprécions énormément."}
							</div>
							<Button bsSize="large" bsStyle="orange">See All Jobs</Button>
						</div>
					</section>
				</div>
				<Footer />
			</div>
		);
	}
}