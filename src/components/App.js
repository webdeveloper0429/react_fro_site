import React from "react";
import "../stylesheets/main.scss";
import {Button} from 'react-bootstrap';
import { bootstrapUtils } from 'react-bootstrap/lib/utils';

bootstrapUtils.addStyle(Button, 'orange');
bootstrapUtils.addStyle(Button, 'blue-green');
bootstrapUtils.addStyle(Button, 'dimGray');
bootstrapUtils.addStyle(Button, 'facebook');
bootstrapUtils.addStyle(Button, 'google');

export default class App extends React.Component {
	render() {
		return (
			<div className="">
				{this.props.children}
			</div>
		);
	}
}
