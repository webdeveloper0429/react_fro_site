import React from "react";
import { Col, Button, FormGroup, ControlLabel, FormControl, HelpBlock} from 'react-bootstrap';
import { Link } from 'react-router';
import Lang from '../../Lang.js';

const isEn = Lang.isEnglish();

function FieldGroup({ id, label, help, ...props }) {
  return (
	<FormGroup controlId={id}>
	  <FormControl {...props} />
	  {help && <HelpBlock>{help}</HelpBlock>}
	</FormGroup>
  );
}
export default class Banner extends React.Component{
	constructor(props) {
		super(props);
		this.register = this.register.bind(this);
		this.state={
			firstNameHelp: '',
			lastNameHelp: '',
			emailHelp: '',
			passwordHelp: '',
			phoneHelp: '',
			cityHelp: '',
		}
	}
	register(){
		if (!$('#firstName').val()) {
			this.setState({firstNameHelp: 'First Name is required!'});
			return false;
		}
		else this.setState({firstNameHelp: ''});

		if (!$('#lastName').val()) {
			this.setState({lastNameHelp: 'Last Name is required!'});
			return false;
		}
		else this.setState({lastNameHelp: ''});

		if (!$('#email').val()) {
			this.setState({emailHelp: 'Email is required!'});
			return false;
		} else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test($('#email').val())) {
			this.setState({emailHelp: 'Invalid email address!'});
			return false;
		}
		else this.setState({emailHelp: ''});

		if (!$('#password').val()) {
			this.setState({passwordHelp: 'Password is required!'});
			return false;
		} else if ($('#password').val().length < 8) {
			this.setState({passwordHelp: 'Password should be more than 8 characters!'});
			return false;
		}
		else this.setState({passwordHelp: ''});

		if (!$('#phone').val()) {
			this.setState({phoneHelp: 'Phone Number is required!'});
			return false;
		} else if (!/^\d+$/i.test($('#phone').val())) {
			this.setState({phoneHelp: 'Invalid phone address!'});
			return false;
		}
		else this.setState({phoneHelp: ''});

		if (!$('#city').val()) {
			this.setState({cityHelp: 'City is required!'});
			return false;
		}
		else this.setState({cityHelp: ''});
	}
	render(){
		return(
			<section className="banner">
				<Col md={6}>
					<div className="title blueGreentext">
						<h1>Work. Life. Balanced.</h1>
					</div>
				</Col>
				<Col md={6}>
					<div className="form">
						<h2>{isEn ? "Become a Pro":"Deviens Pro"}</h2>
						<p className="graytext">{isEn ? "Create an account to get started":"Crée un compte pour commencer"}</p>
						<FieldGroup
							id="firstName"
							type="text"
							placeholder={isEn ? "First Name":"Prénom"}
							help={this.state.firstNameHelp}
						/>
						<FieldGroup
							id="lastName"
							type="text"
							placeholder={isEn ? "Last Name":"Nom de  famille"}
							help={this.state.lastNameHelp}
						/>
						<FieldGroup
							id="email"
							type="email"
							placeholder={isEn ? "Email Address":"Courriel"}
							help={this.state.emailHelp}
						/>
						<FieldGroup
							id="password"
							type="password"
							placeholder={isEn ? "Password":"Mot de passe"}
							help={this.state.passwordHelp}
						/>
						<FieldGroup
							id="phone"
							type="text"
							placeholder={isEn ? "Mobile Phone":"Cellulaire"}
							help={this.state.phoneHelp}
						/>
						<FieldGroup
							id="city"
							type="text"
							placeholder={isEn ? "City":"Ville"}
							help={this.state.cityHelp}
						/>
						<FormGroup>
							<Button bsStyle="blue-green" block onClick={this.register}>{isEn ? "Start Registration":"Inscris-toi"}</Button>
						</FormGroup>
						<p className="graytext">{isEn ? "By signing up you agree to our":"En t'inscrivant tu acceptes nos "} <span className="orangetext"><Link to="/">{isEn ? "Terms of Service":"Termes de Service"}</Link></span> {isEn ? "and ":"et notre"} <span className="orangetext"><Link to="/">{isEn ? "Privacy policy":"Politique de Confidentialité"}</Link></span>.</p>
						<hr/>
						<div className="tail">
							<span className="blueGreentext"><Link to="/login">{isEn ? "Log in to continue":"Connectes-toi pour continuer"}</Link></span>
						</div>
					</div>
				</Col>
			</section>
		)
	}
}