import React from 'react';
import { Col } from 'react-bootstrap';

import Lang from '../../Lang.js';
const isEn = Lang.isEnglish();

export default class How extends React.Component{
	render(){
		return(
			<section className='how'>
				<div className='title'>
					<h2>{isEn ? "How it works":"Comment ça fonctionne?"}</h2>
				</div>
				<div className='content'>
					<Col md={5}>
						<div className='items'>
							<div className='item'>
								<div className='name'>{isEn ? "Mobile App":"App Mobile"}</div>
								<img src='./images/proinfo/mobile.png' width='50' height='50' />
								<img src='./images/proinfo/dots.png' width='99' height='6' />
							</div>
							<div className='item'>
								<div className='name'>{isEn ? "Web Marketplace":"Marché Biface "}</div>
								<img src='./images/proinfo/web.png' width='50' height='50' />
								<img src='./images/proinfo/dots.png' width='99' height='6' />
							</div>
							<div className='item'>
								<div className='name'>Facebook</div>
								<img src='./images/proinfo/facebook.png' width='50' height='50' />
								<img src='./images/proinfo/dots.png' width='99' height='6' />
							</div>
							<div className='item'>
								<div className='name'>Instagram</div>
								<img src='./images/proinfo/instagram.png' width='50' height='50' />
								<img src='./images/proinfo/dots.png' width='99' height='6' />
							</div>
						</div>
					</Col>
					<Col md={2}>
						<img src='./images/proinfo/phone.png' width='200' height='407' />
					</Col>
					<Col md={5}>
						<div className="description">
							<h3>{isEn ? "Client Self Service 24/7":"Libre Service Client 24/7"}</h3>
							<p className='graytext'>{isEn ? "Customize your free profile and allow online booking.  Fact: 45% of all bookings are made outside of typical opening hours.":"Personnalise ton profil gratuit et autorise les réservations en ligne.  Fait:  45% de toutes les réservations sont faites en dehors des heures d'ouvertures typiques."}</p>
						</div>
					</Col>
				</div>
			</section>
		)
	}
}