import React from 'react';
import { Col } from 'react-bootstrap';

import Lang from '../../Lang.js';
const isEn = Lang.isEnglish();

export default class Quote extends React.Component{
	render(){
		return(
			<section className='quote'>
				<div className='title'>
					<h3>{isEn ? "Don't just take our word for it...":"Ne prend pas seulement en compte notre parole..."}</h3>
				</div>
				<div className='content'>
					<Col md={4}>
						<div className='item'>
							<img src='./images/proinfo/ic_quote.png' width='20' height='15' />
							<p className='description'>
								Lorem ipsum dolor sit amet movet libris vocent mel ut, eu quando tur. Libris vocent mel ut.
							</p>
							<img src='./images/proinfo/millena.png' width='60' height='60' />
							<h4>Millena Mill</h4>
						</div>
					</Col>
					<Col md={4}>
						<div className='item'>
							<img src='./images/proinfo/ic_quote.png' width='20' height='15' />
							<p className='description'>
								Lorem ipsum dolor sit amet movet libris vocent mel ut, eu quando tur. Libris vocent mel ut.
							</p>
							<img src='./images/proinfo/john.png' width='60' height='60' />
							<h4>John Doe</h4>
						</div>
					</Col>
					<Col md={4}>
						<div className='item'>
							<img src='./images/proinfo/ic_quote.png' width='20' height='15' />
							<p className='description'>
								Lorem ipsum dolor sit amet movet libris vocent mel ut, eu quando tur. Libris vocent mel ut.
							</p>
							<img src='./images/proinfo/david.png' width='60' height='60' />
							<h4>Steve Bruce</h4>
						</div>
					</Col>
				</div>
			</section>
		)
	}
}