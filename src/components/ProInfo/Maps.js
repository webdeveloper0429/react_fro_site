import React from "react";
import { Button } from 'react-bootstrap';
import { browserHistory } from 'react-router';
import { withGoogleMap, GoogleMap, Marker } from "react-google-maps";

import Lang from '../../Lang.js';
const isEn = Lang.isEnglish();

const SimpleMapExampleGoogleMap = withGoogleMap(props => (
	<GoogleMap
		defaultZoom={17}
		center={props.center}
	>
		<Marker
			defaultPosition={{ lat: 55.756150, lng: 37.6180 }}
			options={{icon: './images/map_pin_orange_s9G_icon.ico'}}
		/>
		<Marker
			defaultPosition={{ lat: 55.755500, lng: 37.6120 }}
			options={{icon: './images/map_pin_orange_s9G_icon.ico'}}
		/>
		<Marker
			defaultPosition={{ lat: 55.755860, lng: 37.6172 }}
			options={{icon: './images/map_pin_orange_s9G_icon.ico'}}
		/>
		<Marker
			defaultPosition={{ lat: 55.755820, lng: 37.6205 }}
			options={{icon: './images/map_pin_orange_s9G_icon.ico'}}
		/>
	</GoogleMap>
));

export default class Maps extends React.Component{
	constructor(props) {
		super(props);
		this.state = {
			zoom: 4,
			center: { lat: 55.755826, lng: 37.6173 },
		};
	}
	learnMore(){
		browserHistory.push('/help');
	}
	render(){
		return(
			<section className="maps">
				<div className="title">
					<h2>{isEn ? "Who uses Fro around me?":"Qui utilise Fro près de moi?"}</h2>
				</div>
				<div className="map">
					<SimpleMapExampleGoogleMap
						containerElement={
						  <div style={{ height: `100%` }} />
						}
						mapElement={
						  <div style={{ height: `100%` }} />
						}
						center={this.state.center}
					/>
				</div>
				<div className="peoples">
					<div className="people">
						<img src="./images/proinfo/john.png" width='80' height='80' />
						<h5>John Doe</h5>
					</div>
					<div className="people">
						<img src="./images/proinfo/millena.png" width='80' height='80' />
						<h5>Millena Mill</h5>
					</div>
					<div className="people">
						<img src="./images/proinfo/david.png" width='80' height='80' />
						<h5>David Steve</h5>
					</div>
					<div className="people">
						<img src="./images/proinfo/sophie.png" width='80' height='80' />
						<h5>Sophie Bruce</h5>
					</div>
				</div>
				<div className='butt'>
					<Button bsSize="large" bsStyle="orange" onClick={this.learnMore}>{isEn ? "Learn More":"En savoir plus"}</Button>
				</div>
			</section>
		)
	}
}