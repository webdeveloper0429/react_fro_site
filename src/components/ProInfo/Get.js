import React from "react";
import {Col, Button} from 'react-bootstrap';
import { browserHistory } from 'react-router';

import Lang from '../../Lang.js';
const isEn = Lang.isEnglish();

export default class Get extends React.Component{
	getStarted(){
		browserHistory.push('/getstarted');
	}
	render(){
		return(
			<section className="get">
				<Col md={7} >
					<div className="description">
						<h1>{isEn ? "New to Fro?":"Fro est nouveau pour toi?"}</h1>
						<p>{isEn ? "Start exploring and discover why people are throwing paper calendars away!":"Commence à explorer et découvre pourquoi les gens jettent leurs calendrier papier!"}</p>
					</div>
				</Col>
				<Col md={5} >
					<div className="button">
						<Button bsSize="large" bsStyle='orange' onClick={this.getStarted}>{isEn ? "Get Started":"Inscription"}</Button>
					</div>
				</Col>
			</section>
		)
	}
}