import React from "react";
import {Col, Media} from 'react-bootstrap';

import Lang from '../../Lang.js';
const isEn = Lang.isEnglish();

export default class Posts extends React.Component{
	render(){
		return(
			<section className='posts'>
				<Col md={1}></Col>
				<Col md={4}>
					<div className="item">
						<Media>
							<Media.Left>
								<img width={40} height={40} src="./images/proinfo/services.png"/>
							</Media.Left>
							<Media.Body>
								<Media.Heading>{isEn ? "24/7 receptionist":"24/7 réceptionniste "}</Media.Heading>
								<p className="graytext">{isEn ? "If you want to enjoy life, you can't be available to your clients 24/7... but Fro can.  Let clients check your free slots and book visits with no interruption to your day.":"Si tu veux profiter de la vie, tu ne peux pas être à la disposition de tes clients 24/7... mais Fro peut. Laisse tes clients vérifier tes dispos et réserver gratuitement en ligne sans interruption à ta journée."}</p>
							</Media.Body>
						</Media>
					</div>
					<div className="item">
						<Media>
							<Media.Left>
								<img width={40} height={40} src="./images/proinfo/no_show_alerts.png"/>
							</Media.Left>
							<Media.Body>
								<Media.Heading>{isEn ? "Reduce no shows":"Réduit les absences"}</Media.Heading>
								<p className="graytext">{isEn ? "See your no-shows decrease by at least 25% with our fee, automated reminder emails and texts.  Your clients won't forget an upcoming appointment again.":"Vois tes absences diminuer d'au moins 25% avec nos courriels et textos de rappel automatisés et gratuits.  Tes clients n'oublieront plus leur prochain rendez-vous."}</p>
							</Media.Body>
						</Media>
					</div>
				</Col>
				<Col md={2}></Col>
				<Col md={4}>
					<div className="item">
						<Media>
							<Media.Left>
								<img width={40} height={40} src="./images/proinfo/new_clients.png"/>
							</Media.Left>
							<Media.Body>
								<Media.Heading>{isEn ? "Meet new clients":"Attire de nouveaux clients"}</Media.Heading>
								<p className="graytext">{isEn ? "Get discovered by creating a public profile to show off your work.  Promote your profile on social media with your own private link and with the great reviews from happy customers.":"Sois découvert en créant un profil public pour mettre en valeur ton travail. Promouvois ton profil sur les réseaux sociaux avec ton lien unique et avec les excellents commentaires de clients satisfaits."}</p>
							</Media.Body>
						</Media>
					</div><div className="item">
						<Media>
							<Media.Left>
								<img width={40} height={40} src="./images/proinfo/grow_business.png"/>
							</Media.Left>
							<Media.Body>
								<Media.Heading>{isEn ? "Grow your revenue":"Augmente tes revenues"}</Media.Heading>
								<p className="graytext">{isEn ? "The easier you make it for clients to book, the more they will book with you.   You get more control and a fuller schedule - without the time consuming phone calls, texts, DM, etc...":"Plus le processus de réservation est simplifié pour tes clients, plus ils réserveront avec toi.  Tu obtiendras plus de contrôle et un calendrier rempli - sans les appels téléphoniques, textos, DM, etc..."}</p>
							</Media.Body>
						</Media>
					</div>
				</Col>
				<Col md={1}></Col>
			</section>
		)
	}
}