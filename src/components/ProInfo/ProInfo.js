import React from "react";
import {Col, Button} from 'react-bootstrap';

import NavBar from '../common/NavBar.js';
import Footer from '../common/Footer.js';

import Banner from './Banner.js';
import Posts from './Posts.js';
import Maps from './Maps.js';
import How from './How.js';
import Quote from './Quote.js';
import Get from './Get.js';

export default class ProInfo extends React.Component{
	render(){
		return(
			<div className="page_container">
				<NavBar />
				<div className="proinfo">
					<Banner />
					<Posts />
					<Maps />
					<How />
					<Quote />
					<Get />
				</div>
				<Footer />
			</div>
		)
	}
}