import React from "react";
import { Link, browserHistory } from 'react-router';
import {Col, Checkbox, Button, FormGroup, ControlLabel, FormControl, HelpBlock, Modal, Alert} from 'react-bootstrap';

import EmptyNav from '../common/EmptyNav.js';
import Footer from '../common/Footer.js'

import Lang from '../../Lang.js';
const isEn = Lang.isEnglish();

function FieldGroup({ id, label, help, ...props }) {
  return (
	<FormGroup controlId={id}>
	  <FormControl {...props} />
	  {help && <HelpBlock>{help}</HelpBlock>}
	</FormGroup>
  );
}
export default class Login extends React.Component {
	constructor(props) {
		super(props);
		this.login = this.login.bind(this);
		this.openModal = this.openModal.bind(this);
		this.closeModal = this.closeModal.bind(this);
		this.sendResetLink = this.sendResetLink.bind(this);
		this.openAlert = this.openAlert.bind(this);
		this.closeAlert = this.closeAlert.bind(this);
		this.state={
			emailHelp: '',
			isModalShow: false,
			resetEmailHelp: '',
			alertVisible: false
		}
	}
	login(){
		if (!$('#email').val()) {
			this.setState({emailHelp: 'Email is required!'});
			return false;
		} else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test($('#email').val())) {
			this.setState({emailHelp: 'Invalid email address!'});
			return false;
		}
		else this.setState({emailHelp: ''});
		browserHistory.push('/');
	}
	openModal(e){
		e.preventDefault();
		this.setState({isModalShow: true});
	}
	closeModal(){
		this.setState({isModalShow: false});
	}
	openAlert(){
		this.setState({alertVisible: true});
	}
	closeAlert(){
		this.setState({alertVisible: false});
	}
	sendResetLink(e){
		e.preventDefault();
		if (!$('#resetEmail').val()) {
			this.setState({resetEmailHelp: 'Email is required!'});
			return false;
		} else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test($('#resetEmail').val())) {
			this.setState({resetEmailHelp: 'Invalid email address!'});
			return false;
		}
		else this.setState({resetEmailHelp: ''});
		this.closeModal();
		this.openAlert();
	}
	render() {
		return (
			<div className="page_container">
				<EmptyNav />
				<div className="login">
					<div className="bg">
						{this.state.alertVisible && <Alert bsStyle="success" onDismiss={this.closeAlert}>
							<p>Email has been sent!</p>
						</Alert>}
						<div className="title">
							<h1>{isEn ? "Log in":"Connexion"}</h1>
						</div>
						<br/><br/>
						<div className="contact_form">
							<form>
								<FormGroup>
									<Button bsSize="large" bsStyle="facebook" block>Facebook</Button>
								</FormGroup>
								<FormGroup>
									<Button bsSize="large" bsStyle="google" block>Google</Button>
								</FormGroup>
								<br/><p style={{textAlign: 'center'}}>or</p><br/>
								<FieldGroup
									id="email"
									type="email"
									placeholder="Email"
									bsSize="large"
									help={this.state.emailHelp}
								/>
								<FieldGroup
									id="password"
									type="password"
									placeholder="Password"
									bsSize="large"
								/>
								<FormGroup>
									<div style={{float: 'left'}}>
										<Checkbox>{isEn ? "Remember me":"Se souvenir de moi"}</Checkbox>
									</div>
									<div style={{float: 'right'}}>
										<Link to="/"><span onClick={this.openModal}>{isEn ? "Forgot Password?":"Mot de passe oublié?"}</span></Link>
									</div>
								</FormGroup>
								<br/><br/>
								<FormGroup>
									<Button bsSize="large" bsStyle="blue-green" block onClick={this.login}>Log in</Button>
								</FormGroup>
								<div style={{textAlign: 'center'}}>
									{isEn ? "Don't have an account?":"Tu n'as pas de compte?"} <Link to="/signup">{isEn ? "Sign Up":"Inscris-toi"}</Link>
								</div>
							</form>
						</div>
					</div>
				</div>
				<Modal show={this.state.isModalShow} onHide={this.closeModal} >
					<Modal.Header closeButton><h3>{isEn ? "Reset Password":"Réinitialiser le mot de passe"}</h3>
					</Modal.Header>
					<Modal.Body>
						<FormGroup>
							<p>{isEn ? "Enter the email address associated with your account, and we’ll email you a link to reset your password.":"Saisissez l'adresse e-mail associée à votre compte. Nous vous enverrons un lien par e-mail pour réinitialiser votre mot de passe."}</p>
						</FormGroup>
						<FieldGroup
							id="resetEmail"
							type="email"
							placeholder="Email"
							bsSize="large"
							help={this.state.resetEmailHelp}
						/>
						<FormGroup>
							<Button bsSize="large" bsStyle="orange" block onClick={this.sendResetLink}>{isEn ? "Send Reset Link":"Envoyer le lien de réinitialisation"}</Button>
						</FormGroup>
					</Modal.Body>
				</Modal>
				<Footer />
			</div>
		);
	}
}
