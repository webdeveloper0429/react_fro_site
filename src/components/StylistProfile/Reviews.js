import React from "react";
import { Row, Col, Pagination } from 'react-bootstrap';
import StarRatingComponent from 'react-star-rating-component';

import Lang from '../../Lang.js';
const isEn = Lang.isEnglish();

class ReviewItem extends React.Component{
	constructor(props) {
		super(props);
	}
	render(){
		return(
			<div className="reviewitem">
				<div className="title">
					<div className="man">
						<img src={this.props.imgSrc} width="50" height="50" />
						<div className="name">
							<h4>{this.props.name}</h4>
							<h5>{this.props.date}</h5>
						</div>
					</div>
					<div className="hands">
						<i className="fa fa-thumbs-down graytext" aria-hidden="true"></i> &nbsp;{this.props.down} &nbsp;&nbsp;&nbsp;
						<i className="fa fa-thumbs-up blueGreentext" aria-hidden="true"></i> &nbsp;{this.props.up} 
					</div>
				</div>
				<div className="introduce">
					{this.props.children}
				</div>
			</div>
		)
	}
}

export default class Reviews extends React.Component {
	constructor(props) {
		super(props);
		this.handleSelect = this.handleSelect.bind(this);
		this.state={
			activePage: 1
		}
	}
	handleSelect(eventKey) {
		this.setState({
			activePage: eventKey
		});
	}
	render() {
		return(
			<div className="reviews" id='reviews'>
				<div className="header">
					<Col sm={5}>
						<div className="score">4.5</div>
						<div className="starrating">
							<StarRatingComponent
								name="app5"
								starColor="#49b7b7"
								emptyStarColor="#49b7b7"
								value={5.5}
								editing={false}
								renderStarIcon={(index, value) => {
								  return <span className={index <= value ? 'fa fa-star' : 'fa fa-star-o'} />;
								}}
								renderStarIconHalf={() => <span className="fa fa-star-half-full" />}
							/>
						</div>
						<h4>{isEn ? "Based on":"Basé sur"} 1,262 {isEn ? "reviews":"commentaires"}</h4>
					</Col>
					<Col sm={7}>
						<div className="subscores">
							<Row>
								<Col xs={6}>
									<h4 className="starlable">{isEn ? "Cleanliness":"Propreté"}</h4>
								</Col>
								<Col xs={6}>
									<div className="starrating">
										<StarRatingComponent
											name="app5"
											starColor="#49b7b7"
											emptyStarColor="#49b7b7"
											value={4}
											editing={false}
											renderStarIcon={(index, value) => {
											  return <span className={index <= value ? 'fa fa-star' : 'fa fa-star-o'} />;
											}}
											renderStarIconHalf={() => <span className="fa fa-star-half-full" />}
										/>
									</div>
								</Col>
							</Row>
							<Row>
								<Col xs={6}>
									<h4 className="starlable">{isEn ? "Communication":"Communication"}</h4>
								</Col>
								<Col xs={6}>
									<div className="starrating">
										<StarRatingComponent
											name="app5"
											starColor="#49b7b7"
											emptyStarColor="#49b7b7"
											value={5.5}
											editing={false}
											renderStarIcon={(index, value) => {
											  return <span className={index <= value ? 'fa fa-star' : 'fa fa-star-o'} />;
											}}
											renderStarIconHalf={() => <span className="fa fa-star-half-full" />}
										/>
									</div>
								</Col>
							</Row>
							<Row>
								<Col xs={6}>
									<h4 className="starlable">{isEn ? "Punctuality":"Ponctualité"}</h4>
								</Col>
								<Col xs={6}>
									<div className="starrating">
										<StarRatingComponent
											name="app5"
											starColor="#49b7b7"
											emptyStarColor="#49b7b7"
											value={5.5}
											editing={false}
											renderStarIcon={(index, value) => {
											  return <span className={index <= value ? 'fa fa-star' : 'fa fa-star-o'} />;
											}}
											renderStarIconHalf={() => <span className="fa fa-star-half-full" />}
										/>
									</div>
								</Col>
							</Row>
							<Row>
								<Col xs={6}>
									<h4 className="starlable">{isEn ? "Service":"Service"}</h4>
								</Col>
								<Col xs={6}>
									<div className="starrating">
										<StarRatingComponent
											name="app5"
											starColor="#49b7b7"
											emptyStarColor="#49b7b7"
											value={5.5}
											editing={false}
											renderStarIcon={(index, value) => {
											  return <span className={index <= value ? 'fa fa-star' : 'fa fa-star-o'} />;
											}}
											renderStarIconHalf={() => <span className="fa fa-star-half-full" />}
										/>
									</div>
								</Col>
							</Row>
						</div>
					</Col>
				</div>
				<hr/>
				<div className="main">
					<ReviewItem 
						imgSrc="./images/stylistprofile/mill.png"
						name="Millena Mill"
						date="September 2016"
						down="0"
						up="20"
					>
						Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
					</ReviewItem>
					<ReviewItem 
						imgSrc="./images/stylistprofile/john.png"
						name="Soophie John"
						date="September 2016"
						down="0"
						up="06"
					>
						Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
					</ReviewItem>
					<Pagination
						prev
						next
						ellipsis
						boundaryLinks
						items={20}
						maxButtons={3}
						activePage={this.state.activePage}
						onSelect={this.handleSelect} 
					/>
				</div>
			</div>
		)
	}
}