import React from "react";
import {Col, Button} from 'react-bootstrap';
import Lightbox from 'react-image-lightbox';

import NavBar from '../common/NavBar.js';
import Footer from '../common/Footer.js';
import TabContent from './TabContent.js';
import TimeTable from './TimeTable.js';

const images = [
	"./images/stylistprofile/hair_1.png",
	"./images/stylistprofile/hair_2.png",
	"./images/stylistprofile/hair_3.png",
	"./images/stylistprofile/hair_4.png",
	"./images/stylistprofile/hair_5.png"
]

export default class StylistProfile extends React.Component {
	constructor(props) {
		super(props);
		this.openImg = this.openImg.bind(this);
		this.state = {
			photoIndex: 0,
			photoOverlay: false,
		}
	}
	componentDidMount() {
		$("nav .btn").addClass('btn-orange');
	}
	openImg(index){
		this.setState({photoIndex: index});
		this.setState({photoOverlay: true});
	}
	render() {
		const {photoIndex, photoOverlay} = this.state;
		return (
			<div className="page_container">
				<NavBar />
				<div className="stylistprofile">
					{photoOverlay &&
						<Lightbox
							mainSrc={images[photoIndex]}
							nextSrc={images[(photoIndex + 1) % images.length]}
							prevSrc={images[(photoIndex + images.length - 1) % images.length]}

							onCloseRequest={() => this.setState({ photoOverlay: false })}
							onMovePrevRequest={() => this.setState({
								photoIndex: (photoIndex + images.length - 1) % images.length,
							})}
							onMoveNextRequest={() => this.setState({
								photoIndex: (photoIndex + 1) % images.length,
							})}
						/>
					}
					<div className="banner">
						<div className="imgs">
							<img src="./images/stylistprofile/hair_1.png" width="125" height="100" onClick={()=>this.openImg(0)}/>
							<img src="./images/stylistprofile/hair_2.png" width="125" height="100" onClick={()=>this.openImg(1)}/>
							<img src="./images/stylistprofile/hair_3.png" width="125" height="100" onClick={()=>this.openImg(2)}/>
							<img src="./images/stylistprofile/hair_4.png" width="125" height="100" onClick={()=>this.openImg(3)}/>
							<img src="./images/stylistprofile/hair_5.png" width="125" height="100" onClick={()=>this.openImg(4)}/>
							<div className="viewmore" onClick={()=>this.openImg(0)}>
								<h4>32</h4>
								<h4>Photos</h4>
							</div>
						</div>
					</div>
					<section className="content">
						<Col lg={8}>
							<TabContent />
						</Col>
						<Col lg={4}>
							<TimeTable openImg={this.openImg}/>
						</Col>
						<div style={{clear: 'both'}}></div> 
					</section>
				</div>
				<Footer />
			</div>
		);
	}
}