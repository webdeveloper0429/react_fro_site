import React from "react";
import { Button, Panel } from "react-bootstrap";

import Lang from '../../Lang.js';
const isEn = Lang.isEnglish();

class Item extends React.Component {
	constructor(props) {
		super(props);
		
	}
	specialEvent(e){
		$(e.currentTarget).parent().parent().find('.panel-collapse').toggleClass('collapse');
	}
	render() {
		return(
			<div className="item">
				<div className="left">
					<h4>{this.props.title}</h4>
					<div className="graytext">
						${this.props.price} and up for {this.props.time} minutes <img src="./images/stylistprofile/info.png" onClick={this.specialEvent}/>
					</div>
					<Panel collapsible>
						<div style={{background: '#eef5f5', padding: '10px'}}>
							{this.props.children}
						</div>
					</Panel>
				</div>
				<div className="right"><Button bsStyle="orange" onClick={()=>{alert('This is Book Button!');}}>&nbsp;&nbsp;&nbsp;{isEn ? "Book":"Réserver"}&nbsp;&nbsp;&nbsp;</Button></div>
				
			</div>
		)
	}
}

export default class Services extends React.Component {
	render() {
		return(
			<div className="services" id='services'>
				<h3> {isEn ? "Book an appointment":"Prend rendez-vous"} </h3>
				<Panel collapsible defaultExpanded header="Hair Color" >
					<Item title="All over color" price="120" time="150">Cleansing and styling chemically treated hair.</Item>
					<hr/>
					<Item title="Balayage" price="175" time="240">Cleansing CHEMICALLY TREATED hair and applying a deep conditioning treatment to be placed under the hydration machine and then styled</Item>
					<hr/>
					<Item title="All over color" price="120" time="150">A "Specialty" Set is considered a twist, rod, spiral or two stranded set.</Item>
					<hr/>
					<Item title="Balayage" price="175" time="240">Cleansing and styling chemically treated hair.</Item>
				</Panel>
				<Panel collapsible defaultExpanded header="Sew-Ins" >
					<Item title="All over color" price="120" time="150">Cleansing CHEMICALLY TREATED hair and applying a deep conditioning treatment to be placed under the hydration machine and then styled</Item>
					<hr/>
					<Item title="Balayage" price="175" time="240">A "Specialty" Set is considered a twist, rod, spiral or two stranded set.</Item>
					<hr/>
					<Item title="All over color" price="120" time="150">Cleansing and styling chemically treated hair.</Item>
				</Panel>
				<Panel collapsible defaultExpanded header="Deals" >
					<Item title="All over color" price="120" time="150">Cleansing CHEMICALLY TREATED hair and applying a deep conditioning treatment to be placed under the hydration machine and then styled</Item>
					<hr/>
					<Item title="Balayage" price="175" time="240">A "Specialty" Set is considered a twist, rod, spiral or two stranded set.</Item>
				</Panel>
			</div>
		)
	}
}