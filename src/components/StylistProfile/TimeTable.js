import React from "react";
import { Col, Modal, Glyphicon } from "react-bootstrap";
import { Link } from "react-router";
import Lightbox from 'react-image-lightbox';
import BigCalendar from 'react-big-calendar';
import 'react-big-calendar/lib/css/react-big-calendar.css';
import moment from 'moment';
import Data from '../common/Data.js';

import Lang from '../../Lang.js';
const isEn = Lang.isEnglish();

BigCalendar.setLocalizer(
  BigCalendar.momentLocalizer(moment)
);

const images = [
	"./images/stylistprofile/hair_1.png",
	"./images/stylistprofile/hair_2.png",
	"./images/stylistprofile/hair_3.png",
	"./images/stylistprofile/hair_4.png",
	"./images/stylistprofile/hair_5.png"
]

export default class TimeTable extends React.Component{
	constructor(props) {
		super(props);
		this.openAvailability = this.openAvailability.bind(this);
		this.closeAvailability = this.closeAvailability.bind(this);
		this.openShare = this.openShare.bind(this);
		this.closeShare = this.closeShare.bind(this);
		this.state = {
			availabilityModal: false,
			shareModal: false
		}
	}
	openAvailability(e){
		e.preventDefault();
		this.setState({availabilityModal: true});
	}
	closeAvailability(){
		this.setState({availabilityModal: false});
	}
	openShare(e){
		e.preventDefault();
		this.setState({shareModal: true});
	}
	closeShare(){
		this.setState({shareModal: false});
	}
	render() {
		const min = new Date();
				min.setHours(8);
				min.setMinutes(0, 0, 0);

		const max = new Date();
				max.setHours(22);
				max.setMinutes(0, 0, 0);
		return(
			<div className="overlay">
				<div className="timetable">
					<div className="imgs">
						<img src="./images/stylistprofile/hair_1.png" width="125" height="100" onClick={()=>this.props.openImg(0)}/>
						<img src="./images/stylistprofile/hair_2.png" width="125" height="100" onClick={()=>this.props.openImg(1)}/>
						<img src="./images/stylistprofile/hair_3.png" width="125" height="100" onClick={()=>this.props.openImg(2)}/>
						<img src="./images/stylistprofile/hair_4.png" width="125" height="100" onClick={()=>this.props.openImg(3)}/>
						<img src="./images/stylistprofile/hair_5.png" width="125" height="100" onClick={()=>this.props.openImg(4)}/>
						<div className="viewmore" onClick={()=>this.props.openImg(0)}>
							<h4>32</h4>
							<h4>Photos</h4>
						</div>
					</div>
					<div className="times">
						<h4>{isEn ? "Business Hours":"Heures Disponibles"}</h4>
						<div className="tables">
							<div className="rows">
								<h5>{isEn ? "Sunday":"Dimanche"}:</h5>
								<h5>{isEn ? "Closed":"Fermé"}</h5>
							</div>
							<div className="rows">
								<h5>{isEn ? "Monday":"Lundi"}:</h5>
								<h5>{isEn ? "Closed":"Fermé"}</h5>
							</div>
							<div className="rows">
								<h5>{isEn ? "Tuesday":"Mardi"}:</h5>
								<h5>10:00 AM - 8:00 PM</h5>
							</div>
							<div className="rows">
								<h5>{isEn ? "Wednesday":"Mercredi"}:</h5>
								<h5>10:00 AM - 8:00 PM</h5>
							</div>
							<div className="rows">
								<h5>{isEn ? "Thursday":"Jeudi"}:</h5>
								<h5>10:00 AM - 8:00 PM</h5>
							</div>
							<div className="rows">
								<h5>{isEn ? "Friday":"Vendredi"}:</h5>
								<h5>10:00 AM - 8:00 PM</h5>
							</div>
							<div className="rows">
								<h5>{isEn ? "Saturday":"Samedi"}:</h5>
								<h5>10:00 AM - 6:00 PM</h5>
							</div>
						</div>
					</div>
				</div>
				<div className="actions">
					<Link to="#">
						<div className="item" onClick={this.openAvailability}>
							<Col xs={4}>
								<div className="orangetext"><i className="fa fa-calendar" aria-hidden="true"></i></div>
							</Col>
							<Col xs={8}>
								{isEn ? "Availability":"Disponibilités"}
							</Col>
						</div>
					</Link>
					<hr/>
					<Link to="#">
						<div className="item">
							<Col xs={4}>
								<div className="orangetext"><i className="fa fa-heart-o" aria-hidden="true"></i></div>
							</Col>
							<Col xs={8}>
								{isEn ? "Save Profile":"Favori"}
							</Col>
						</div>
					</Link>
					<hr/>
					<Link to="#">
						<div className="item"  onClick={this.openShare}>
							<Col xs={4}>
								<div className="orangetext"><i className="fa fa-share-alt" aria-hidden="true"></i></div>
							</Col>
							<Col xs={8}>
								{isEn ? "Share Profile":"Partager "}
							</Col>
						</div>
					</Link>
				</div>
				<div className="report graytext">
					<Link to="#"><i className="fa fa-flag-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;{isEn ? "Report this listing":"Signaler cette annonce"}</Link>
				</div>
				<Modal show={this.state.shareModal} onHide={this.closeShare}>

				</Modal>
				<Modal show={this.state.availabilityModal} bsSize='large'  onHide={this.closeAvailability}>
					<Modal.Header closeButton className="blueGreenHeader">
						<h3>Availability</h3>
					</Modal.Header>
					<div className="modalsubheader">
						<h4>Depending on the service, availability may change</h4>
					</div>
					<Modal.Body>
						<BigCalendar
							selectable
							events={Data.events}
							step={15}
							timeslots={1}
							min={min}
							max={max}
							components={{
								toolbar: CustomToolbar
							}}
							defaultView='week'
							scrollToTime={new Date(2017, 4, 12, 9, 30)}
							defaultDate={new Date()}
							onSelectEvent={event => alert(event.title)}
						/>
					</Modal.Body>
					
				</Modal>
			</div>
		)
	}
}

const CustomToolbar = (toolbar) => {
	const goToBack = () => {
		const today = new Date();
		const current = toolbar.date;
		if(today.getTime() >= current.getTime()) return false;
		toolbar.date.setDate(toolbar.date.getDate() - 7);
		toolbar.onNavigate('prev');
	};

	const goToNext = () => {
		toolbar.date.setDate(toolbar.date.getDate() + 7);
		toolbar.onNavigate('next');
	};

	// const goToCurrent = () => {
	// 	const now = new Date();
	// 	toolbar.date.setMonth(now.getMonth());
	// 	toolbar.date.setYear(now.getFullYear());
	// 	toolbar.onNavigate('current');
	// };

	const label = () => {
		const date = moment(toolbar.date);
		return (
			<span><b>{date.format('MMMM')}</b><span> {date.format('YYYY')}</span></span>
		);
	};

	return (
		<div className="customToolbar">
			<Glyphicon glyph="chevron-left" onClick={goToBack}/>
			<label >{label()}</label>
			<Glyphicon glyph="chevron-right" onClick={goToNext}/>
		</div>
	);
};