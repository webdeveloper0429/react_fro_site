import React from "react";
import { Link } from "react-router";
import { Row, Col, Button, Modal, FormGroup, ControlLabel, FormControl, HelpBlock} from 'react-bootstrap';
import StarRatingComponent from 'react-star-rating-component';
import Avatar from 'react-avatar';

import Lang from '../../Lang.js';
const isEn = Lang.isEnglish();

function FieldGroup({ id, label, help, ...props }) {
  return (
	<FormGroup controlId={id}>
	  <FormControl {...props} />
	  {help && <HelpBlock>{help}</HelpBlock>}
	</FormGroup>
  );
}

export default class Overview extends React.Component{
	constructor() {
		super();
		this.openContact = this.openContact.bind(this);
		this.closeContact = this.closeContact.bind(this);
		this.sendMessage = this.sendMessage.bind(this);
		this.state = {
			rating: 5.5,
			contactModal: false,
			messageHelp: ''
		};
	}
	openContact(e){
		e.preventDefault();
		this.setState({contactModal: true});
	}
	closeContact(){
		this.setState({contactModal: false});
	}
	sendMessage(){
		if (!$('#message').val()) {
			this.setState({messageHelp: 'Message is required!'});
			return false;
		}
		else this.setState({messageHelp: ''});

		this.closeContact()
		alert("Your message is \n"+$('#message').val()+'\n\n'+'file is \n'+$('#file').val());
	}
	onStarClickHalfStar(nextValue, prevValue, name) {
	}
	render() {
		return(
			<div className="overview" id='overview'>
				<div className="header">
					<div className="man">
						<div className="photo">
							<img src="./images/stylistprofile/david.png" width="150" height="150" />
						</div>
						<div className="name">
							<h3>{isEn ? "Hairstyled by":"Coiffure par"} <span className="orangetext"><Link to='/'>David</Link></span></h3>
							<h4>{isEn ? "I host at my home":"Je reçois chez moi"}</h4>
						</div>
					</div>
					<div className="review">
						<div className="stars">
							<StarRatingComponent
								name="app5"
								starColor="#49b7b7"
								emptyStarColor="#49b7b7"
								value={this.state.rating}
								editing={false}
								renderStarIcon={(index, value) => {
								  return <span className={index <= value ? 'fa fa-star' : 'fa fa-star-o'} />;
								}}
								renderStarIconHalf={() => <span className="fa fa-star-half-full" />}
							/>
						</div>
						<h6 className="countreview">37 Reviews</h6>
					</div>
				</div>
				<div className="introduce">
					<h3>{isEn ? "About me":"À propos de moi"}</h3>
					<div className="description">
						Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
						<br/><br/>
						Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
					</div>
					<h3 className="contactme">
						<Link to='/' onClick={this.openContact}>{isEn ? "Contact":"Contacte"} David</Link>
					</h3>
				</div>
				<Modal show={this.state.contactModal} onHide={this.closeContact} >
					<Modal.Header closeButton className="grayHeader">
						<h4>Contact David</h4>
					</Modal.Header>
					<div className="custom_modal_body">
						<Col sm={4} className="sideBar">
							<Avatar 
								name="Foo Bar" 
								round={true}
								size={150}
								src='./images/stylistprofile/david.png'
							/>
						</Col>
						<Col sm={8} className="main">
							<FieldGroup
								componentClass="textarea"
								id="message"
								rows="10"
								placeholder="Write a message..."
								bsSize="large"
								help={this.state.messageHelp}
							/>
							<FormGroup>
								<FormControl
									type='file'
									id='file'
								/>
							</FormGroup>
							<div className="tail">
								<Avatar 
									name="Sending Person" 
									round={true}
									size={50}
									src='./images/stylistprofile/mill.png'
								/>
								<Button bsStyle="orange" bsSize="large" onClick={this.sendMessage}>Send Message</Button>
							</div>
						</Col>
					</div>
				</Modal>
			</div>
		)
	}
}