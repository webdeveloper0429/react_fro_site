import React from "react";

import Overview from './Overview.js';
import Services from './Services.js';
import Reviews from './Reviews.js';
import Location from './Location.js'

import Lang from '../../Lang.js';
const isEn = Lang.isEnglish();

export default class TabContent extends React.Component {

	componentDidMount() {

		$('#onenav').onePageNav();
	}
	render() {
		return(
			<div className="singlepagetab">
				<ul id="onenav">
					<li className="current"><a href="#overview">{isEn ? "Overview":"Présentation"}</a></li>
					<li><a href="#services">{isEn ? "Services":"Services"}</a></li>
					<li><a href="#reviews">{isEn ? "Reviews":"Commentaires"}</a></li>
					<li><a href="#location">{isEn ? "Location":"Emplacement"}</a></li>
				</ul>
				<Overview />
				<div className="sharethis-inline-share-buttons"></div>
				<hr/>
				<Services />
				<hr/>
				<Reviews />
				<hr/>
				<Location />
			</div>
		)
	}
}