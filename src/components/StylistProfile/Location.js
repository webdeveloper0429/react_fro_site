import React from "react";

import { withGoogleMap, GoogleMap, Marker } from "react-google-maps";

import Lang from '../../Lang.js';
const isEn = Lang.isEnglish();

const SimpleMapExampleGoogleMap = withGoogleMap(props => (
	<GoogleMap
		defaultZoom={15}
		center={props.center}
	>
		<Marker
			defaultPosition={props.center}
			options={{icon: './images/map_pin_orange_s9G_icon.ico'}}
		/>
	</GoogleMap>
));

export default class Location extends React.Component{
	constructor(props) {
		super(props);
		this.state = {
			zoom: 4,
			center: { lat: 55.755826, lng: 37.6173 },
		};
	}
	render(){
		return(
			<div className="location" id='location'>
				<SimpleMapExampleGoogleMap
					containerElement={
					  <div style={{ height: `100%` }} />
					}
					mapElement={
					  <div style={{ height: `100%` }} />
					}
					center={this.state.center}
				/>
				<h5>{isEn ? "Exact location information is provided after an appointment is confirmed":"L'adresse exacte est communiquée uniquement lorsque la réservation est confirmée"}</h5>
			</div>
		)
	}
}