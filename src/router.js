import React from "react";
import { Router, Route, IndexRoute } from "react-router";
import { history } from "./store.js";
import App from "./components/App";
import Home from "./components/HomePage/HomePage.js";
import Download from "./components/Download/Download.js";
import ContactUs from "./components/ContactUs/ContactUs.js";
import Login from "./components/Login/Login.js";
import SignUp from "./components/SignUp/SignUp.js";
import Help from "./components/Help/Help.js";
import OurStory from "./components/OurStory/OurStory.js";
import StylistProfile from "./components/StylistProfile/StylistProfile.js";
import ProInfo from "./components/ProInfo/ProInfo.js";
import GetStarted from "./components/GetStarted/GetStarted.js";
import NotFound from "./components/NotFound";

// build the router
const router = (
	<Router onUpdate={() => window.scrollTo(0, 0)} history={history}>
		<Route path="/" component={App}>
			<IndexRoute component={Home}/>
			<Route path="download" component={Download}/>
			<Route path="contactus" component={ContactUs}/>
			<Route path="login" component={Login}/>
			<Route path="signup" component={SignUp}/>
			<Route path="help" component={Help}/>
			<Route path="ourstory" component={OurStory}/>
			<Route path="stylistprofile" component={StylistProfile}/>
			<Route path="proinfo" component={ProInfo}/>
			<Route path="getstarted" component={GetStarted}/>
			<Route path="*" component={NotFound}/>
		</Route>
	</Router>
);

// export
export { router };
