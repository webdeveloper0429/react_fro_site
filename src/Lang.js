import localStorage from 'localStorage';
class Lang {

  static setFrance() {
    localStorage.setItem('france', true);
  }

  static isEnglish() {
    return localStorage.getItem('france') == null;
  }

  static setEnglish() {
    localStorage.removeItem('france');
  }

}

export default Lang;